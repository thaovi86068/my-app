/* eslint-disable jsx-a11y/img-redundant-alt */
/* eslint-disable jsx-a11y/anchor-is-valid  */
import { useDispatch } from 'react-redux';
import { choooseVoice } from '../redux/messagesSlice';
const Header = () => {
  //const [voice, setVoice] = useState('');
  // const voice = useSelector((state) => state.messages.voice);
  const dispatch = useDispatch();
  // useEffect(() => {
  //   console.log(voice);
  // }, [voice]);

  return (
    <div className="card-header">
      <div className="box-header with-border d-flex justify-content-between">
        <div className="row justify-content-between w-100 px-0 mx-0">
          <div className="col-10 d-flex justify-content-start">
            <img src="./dino.png" alt={'logo'} style={{ width: '100%' }} />
          </div>
          <div className="col-2 d-flex flex-column align-item-center justify-content-center px-0 mx-0">
            <div className="btn-group">
              <button
                className="btn btn-white dropdown-toggle"
                type="button"
                data-toggle="dropdown"
                id="chon_giong_doc"
                aria-expanded="false"
              >
                <i className="fas fa-cog"></i>
              </button>
              <div className="dropdown-menu" aria-labelledby="chon_giong_doc">
                <a
                  className="dropdown-item"
                  href="\#"
                  onClick={() => dispatch(choooseVoice('hn-thanhtung'))}
                >
                  Giọng đọc miền bắc
                </a>
                <a
                  className="dropdown-item"
                  href="\#"
                  onClick={() => dispatch(choooseVoice('hue-baoquoc'))}
                >
                  Giọng đọc miền trung
                </a>
                <a
                  className="dropdown-item"
                  href="\#"
                  onClick={() => dispatch(choooseVoice('nguyenthithuyduyen'))}
                >
                  Giọng đọc miền nam
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Header;
