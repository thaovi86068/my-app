import AudioPlayer from 'react-h5-audio-player';
import 'react-h5-audio-player/lib/styles.css';

const Message = ({ isBot, text, url }) => {
  const today = new Date();
  const date =
    today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
  const time =
    today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds();
  const dateTime = date + ' | ' + time;

  return (
    <div className={isBot ? 'direct-chat-msg' : 'direct-chat-msg right'}>
      <div className="direct-chat-info clearfix">
        <span
          className={
            isBot
              ? 'direct-chat-name float-left mx-1'
              : 'direct-chat-name float-right mx-1'
          }
        >
          {isBot ? 'PJICO' : 'YOU'}
        </span>
        <span
          className={
            isBot
              ? 'direct-chat-timestamp float-right mx-1'
              : 'direct-chat-timestamp float-left mx-1'
          }
        >
          {dateTime}
        </span>
      </div>
      <img
        className="direct-chat-img"
        src={
          isBot
            ? 'https://img.icons8.com/color/36/000000/administrator-male.png'
            : 'https://img.icons8.com/office/36/000000/person-female.png'
        }
        alt={'avatar'}
      />
      <div className="direct-chat-text">
        {url !== '#' && isBot ? (
          <div className="row">
            <div className="col-3 d-flex justify-content-center align-items-start">
              <AudioPlayer
                src={url}
                showJumpControls={false}
                showSkipControls={false}
                customVolumeControls={[]}
                customAdditionalControls={[]}
                autoPlay={true}
              />
            </div>
            <div className="col-9"></div>
            <div className="col-12">{text}</div>
          </div>
        ) : (
          <div className="row">
            <div className="col">{text}</div>
          </div>
        )}
      </div>
    </div>
  );
};

export default Message;
