import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';

getSession();
async function getSession() {
    //
    // link api lấy session
    //https://bot.xbot.vn/chatbot/0c4ff4de-9172-4cb9-9583-689abb6ffe24
    const vURL = `https://128.199.143.246/chatbot/0c4ff4de-9172-4cb9-9583-689abb6ffe24`;
    const vRequest = await fetch(vURL);
    const vRespone = await vRequest.json();
    if (vRespone.success) {
        sessionStorage.setItem('sessionId', vRespone.data.context.sessionId);
    }
}

export const fetchApi = createAsyncThunk('chat/fetchChat', async (text) => {
    // khai báo dữ liệu sẽ gửi lên api
    const payload = {
        actionType: 'INPUT',
        data: {
            id:
                'user-message' +
                '-' +
                Date.now() +
                '-' +
                Math.floor(Math.random(0, 100000000) * 100000000),
            type: 'UserText',
            content: {
                text: text.text,
            },
        },
    };

    // khai báo URL
    const vURL = `https://128.199.143.246/chatbot/chat/${sessionStorage.getItem('sessionId')}`;

    //gửi api
    const request = await fetch(vURL, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json;charset=UTF-8',
        },
        body: JSON.stringify(payload),
    });
    const respone = await request.json();
    return respone;
});

// xử lý các action tại đây
const messagesSlice = createSlice({
    name: 'messages',
    initialState: {
        voice: 'hn-thanhtung',
        dataToSpeechApi: {
            text: '',
        },
        messages: [{ text: 'Xin chào hãy hỏi tôi gì đó đi! ', isBot: true, url: '#' }],
    },
    reducers: {
        updateMessagesState: (state, action) => {
            const newMessages = [...state.messages, { ...action.payload }];
            return { ...state, messages: [...newMessages] };
        },
        choooseVoice: (state, action) => {
            state.voice = action.payload;
        },
    },
    extraReducers: {
        [fetchApi.pending]: (state, action) => {
            // console.log('pending');
        },
        [fetchApi.fulfilled]: (state, { payload }) => {
            // sanitize html string from xbot and get textContent
            function extractContent(paramTextXbot) {
                let span = document.createElement('span');
                span.innerHTML = paramTextXbot;
                return span.textContent || span.innerText;
            }
            if (Object.keys(payload.data).length > 0) {
                const reponseFromApi = {
                    text: extractContent(payload.data[0].message.content.text),
                    isBot: true,
                    url: '',
                };
                state.messages.push(reponseFromApi);
                const vLastElementInMessagesArray = state.messages.at(-1);
                state.dataToSpeechApi.text = vLastElementInMessagesArray.text;
            }
        },
        [fetchApi.rejected]: (state, action) => {
            console.log('rejected');
        },
    },
});

export const { updateMessagesState, choooseVoice } = messagesSlice.actions;

export default messagesSlice.reducer;
